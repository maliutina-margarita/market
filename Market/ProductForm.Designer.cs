﻿namespace Market
{
    partial class ProductsForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductsForm));
            this.label1 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.productComboBox = new System.Windows.Forms.ComboBox();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.sortComboBox = new System.Windows.Forms.ComboBox();
            this.sortButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.refreshButton = new System.Windows.Forms.Button();
            this.ProductListBox = new System.Windows.Forms.ListBox();
            this.AddToCartButton = new System.Windows.Forms.Button();
            this.CountNumber = new System.Windows.Forms.NumericUpDown();
            this.AddCountLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CountLabel = new System.Windows.Forms.Label();
            this.UserTypeLabel = new System.Windows.Forms.Label();
            this.CartButton = new System.Windows.Forms.Button();
            this.PreviousOrdersButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.CountNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(411, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(258, 88);
            this.label1.TabIndex = 1;
            this.label1.Text = "Products";
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.White;
            this.addButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("addButton.BackgroundImage")));
            this.addButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addButton.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addButton.Location = new System.Drawing.Point(218, 114);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(165, 58);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Add";
            this.addButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.AddButtonClick);
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.deleteButton.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.deleteButton.Location = new System.Drawing.Point(837, 195);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(123, 51);
            this.deleteButton.TabIndex = 3;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButtonClick);
            // 
            // productComboBox
            // 
            this.productComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.productComboBox.FormattingEnabled = true;
            this.productComboBox.Location = new System.Drawing.Point(608, 207);
            this.productComboBox.Name = "productComboBox";
            this.productComboBox.Size = new System.Drawing.Size(213, 28);
            this.productComboBox.TabIndex = 5;
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(747, 369);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(213, 26);
            this.searchTextBox.TabIndex = 6;
            this.searchTextBox.TextChanged += new System.EventHandler(this.SearchTextBoxTextChanged);
            // 
            // sortComboBox
            // 
            this.sortComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sortComboBox.FormattingEnabled = true;
            this.sortComboBox.Location = new System.Drawing.Point(608, 289);
            this.sortComboBox.Name = "sortComboBox";
            this.sortComboBox.Size = new System.Drawing.Size(213, 28);
            this.sortComboBox.TabIndex = 9;
            // 
            // sortButton
            // 
            this.sortButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sortButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sortButton.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sortButton.Location = new System.Drawing.Point(837, 277);
            this.sortButton.Name = "sortButton";
            this.sortButton.Size = new System.Drawing.Size(123, 51);
            this.sortButton.TabIndex = 8;
            this.sortButton.Text = "Sort";
            this.sortButton.UseVisualStyleBackColor = false;
            this.sortButton.Click += new System.EventHandler(this.SortButtonClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(592, 351);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 64);
            this.label2.TabIndex = 10;
            this.label2.Text = "Search";
            // 
            // refreshButton
            // 
            this.refreshButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.refreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.refreshButton.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.refreshButton.Location = new System.Drawing.Point(837, 12);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(123, 51);
            this.refreshButton.TabIndex = 11;
            this.refreshButton.Text = "Refresh";
            this.refreshButton.UseVisualStyleBackColor = false;
            this.refreshButton.Click += new System.EventHandler(this.RefreshButtonClick);
            // 
            // ProductListBox
            // 
            this.ProductListBox.FormattingEnabled = true;
            this.ProductListBox.ItemHeight = 20;
            this.ProductListBox.Location = new System.Drawing.Point(63, 203);
            this.ProductListBox.Name = "ProductListBox";
            this.ProductListBox.Size = new System.Drawing.Size(486, 304);
            this.ProductListBox.TabIndex = 12;
            this.ProductListBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ProductListBoxMouseClick);
            this.ProductListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ProductListBoxMouseDoubleClick);
            // 
            // AddToCartButton
            // 
            this.AddToCartButton.BackColor = System.Drawing.Color.White;
            this.AddToCartButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.AddToCartButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AddToCartButton.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddToCartButton.Location = new System.Drawing.Point(190, 736);
            this.AddToCartButton.Name = "AddToCartButton";
            this.AddToCartButton.Size = new System.Drawing.Size(199, 81);
            this.AddToCartButton.TabIndex = 13;
            this.AddToCartButton.Text = "Add to cart";
            this.AddToCartButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddToCartButton.UseVisualStyleBackColor = false;
            this.AddToCartButton.Click += new System.EventHandler(this.AddToCartButtonClick);
            // 
            // CountNumber
            // 
            this.CountNumber.Location = new System.Drawing.Point(218, 669);
            this.CountNumber.Name = "CountNumber";
            this.CountNumber.Size = new System.Drawing.Size(155, 26);
            this.CountNumber.TabIndex = 14;
            this.CountNumber.ValueChanged += new System.EventHandler(this.CountNumberValueChanged);
            // 
            // AddCountLabel
            // 
            this.AddCountLabel.AutoSize = true;
            this.AddCountLabel.BackColor = System.Drawing.Color.Transparent;
            this.AddCountLabel.Font = new System.Drawing.Font("Segoe Print", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddCountLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.AddCountLabel.Location = new System.Drawing.Point(217, 602);
            this.AddCountLabel.Name = "AddCountLabel";
            this.AddCountLabel.Size = new System.Drawing.Size(136, 64);
            this.AddCountLabel.TabIndex = 15;
            this.AddCountLabel.Text = "Count";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe Print", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(86, 538);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(303, 64);
            this.label4.TabIndex = 16;
            this.label4.Text = "Available count";
            // 
            // CountLabel
            // 
            this.CountLabel.AutoSize = true;
            this.CountLabel.BackColor = System.Drawing.Color.Transparent;
            this.CountLabel.Font = new System.Drawing.Font("Segoe Print", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CountLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.CountLabel.Location = new System.Drawing.Point(404, 538);
            this.CountLabel.Name = "CountLabel";
            this.CountLabel.Size = new System.Drawing.Size(54, 64);
            this.CountLabel.TabIndex = 17;
            this.CountLabel.Text = "0";
            // 
            // UserTypeLabel
            // 
            this.UserTypeLabel.AutoSize = true;
            this.UserTypeLabel.BackColor = System.Drawing.Color.Transparent;
            this.UserTypeLabel.Font = new System.Drawing.Font("Segoe Print", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UserTypeLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UserTypeLabel.Location = new System.Drawing.Point(12, 9);
            this.UserTypeLabel.Name = "UserTypeLabel";
            this.UserTypeLabel.Size = new System.Drawing.Size(60, 71);
            this.UserTypeLabel.TabIndex = 18;
            this.UserTypeLabel.Text = "...";
            // 
            // CartButton
            // 
            this.CartButton.BackColor = System.Drawing.Color.White;
            this.CartButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CartButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CartButton.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CartButton.Location = new System.Drawing.Point(738, 557);
            this.CartButton.Name = "CartButton";
            this.CartButton.Size = new System.Drawing.Size(199, 81);
            this.CartButton.TabIndex = 19;
            this.CartButton.Text = "Cart";
            this.CartButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CartButton.UseVisualStyleBackColor = false;
            this.CartButton.Click += new System.EventHandler(this.CartButtonClick);
            // 
            // PreviousOrdersButton
            // 
            this.PreviousOrdersButton.BackColor = System.Drawing.Color.White;
            this.PreviousOrdersButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PreviousOrdersButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.PreviousOrdersButton.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PreviousOrdersButton.Location = new System.Drawing.Point(738, 695);
            this.PreviousOrdersButton.Name = "PreviousOrdersButton";
            this.PreviousOrdersButton.Size = new System.Drawing.Size(199, 81);
            this.PreviousOrdersButton.TabIndex = 20;
            this.PreviousOrdersButton.Text = "View my previous orders";
            this.PreviousOrdersButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.PreviousOrdersButton.UseVisualStyleBackColor = false;
            // 
            // ProductsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1017, 909);
            this.Controls.Add(this.PreviousOrdersButton);
            this.Controls.Add(this.CartButton);
            this.Controls.Add(this.UserTypeLabel);
            this.Controls.Add(this.CountLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.AddCountLabel);
            this.Controls.Add(this.CountNumber);
            this.Controls.Add(this.AddToCartButton);
            this.Controls.Add(this.ProductListBox);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sortComboBox);
            this.Controls.Add(this.sortButton);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.productComboBox);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.label1);
            this.Name = "ProductsForm";
            this.Text = "Products";
            ((System.ComponentModel.ISupportInitialize)(this.CountNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.ComboBox productComboBox;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.ComboBox sortComboBox;
        private System.Windows.Forms.Button sortButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.ListBox ProductListBox;
        private System.Windows.Forms.Button AddToCartButton;
        private System.Windows.Forms.NumericUpDown CountNumber;
        private System.Windows.Forms.Label AddCountLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label CountLabel;
        private System.Windows.Forms.Label UserTypeLabel;
        private System.Windows.Forms.Button CartButton;
        private System.Windows.Forms.Button PreviousOrdersButton;
    }
}

