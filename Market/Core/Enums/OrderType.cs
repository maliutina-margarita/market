﻿namespace Market.Core.Enums
{
    public enum OrderType
    {
        Asc, Desc
    }
}
