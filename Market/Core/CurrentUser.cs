﻿using Market.Entity;

namespace Market.Core
{
    public class CurrentUser
    {
        public static User User { get; set; }
    }
}
