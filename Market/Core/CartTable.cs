﻿namespace Market.Core
{
    public class CartTable
    {
        public int OrderId { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public int Price { get; set; }
        
        public int TotalPrice { get; set; }
    }
}
