﻿using System;
using System.Windows.Forms;

namespace Market
{
    using Market.Repository;

    public partial class RegistrationForm : Form
    {
        private readonly RegistrationRepository repository  = new RegistrationRepository();

        public RegistrationForm()
        {
            InitializeComponent();

            repository.CreateDefaultUserType(this.UserTypeComboBox.Text);

            this.UserTypeComboBox.DataSource = this.repository.GetAllUserTypes();
            this.UserTypeComboBox.DisplayMember = "Name";
            this.UserTypeComboBox.ValueMember = "Id";
        }

        private void Button1Click(object sender, EventArgs e)
        {
            this.repository.Registration(this.EmailTextBox.Text, this.PasswordTextBox.Text, this.UserTypeComboBox.Text);
            this.Close();
        }

        private void ReturnButtonClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
