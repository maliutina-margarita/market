﻿using System;
using System.Windows.Forms;
using Market.Repository;

namespace Market
{
    public partial class InfoForm : Form
    {
       private readonly ProductInfoRepository repository = new ProductInfoRepository();

        public InfoForm()
        {
            InitializeComponent();
        }

        private void ReturnButtonClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            this.repository.AddProductToProductList(this.NameTextBox.Text, int.Parse(this.CountTextBox.Text), int.Parse(this.PriceTextBox.Text), this.DescriptionTextBox.Text, this.ProviderTextBox.Text, int.Parse(this.WeightTextBox.Text));
            MessageBox.Show("Инормация добавлена");
            this.Close();
        }
    }
}
