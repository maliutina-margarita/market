﻿using System.Linq;
using Market.Core;
using Market.Entity;

namespace Market.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Windows.Forms;

    public class CartRepository
    {
        private readonly Context context = new Context();

        public Cart GetUserCart()
        {
            return this.context.Carts.FirstOrDefault(c => c.Id == CurrentUser.User.CartId);
        }

        public List<CartTable> GetListOfOrders()
        {
            var currentCart = CurrentUser.User.Cart.Id;
            return context.Orders.Where(o => o.CartId == currentCart).Select(r => new CartTable
            {
               OrderId = r.Id,
               Name = r.Product.Name,
               Count = r.Count,
               Price = r.Product.Price,
               TotalPrice = r.TotalPrice
            }).ToList();
        }

        public void DeleteOrderFromCartTable(int id)
        {
            var currentOrder = this.context.Orders.FirstOrDefault(o => o.Id == id);
            var currentProduct = this.context.Products.FirstOrDefault(p => p.Id == currentOrder.ProductId);
            if (currentOrder != null && currentProduct != null)
            {
                currentProduct.Count = currentProduct.Count + currentOrder.Count;
                this.context.Orders.Remove(currentOrder);
                this.context.SaveChanges();
                MessageBox.Show("Продукт удален");
            }
            else
            {
                MessageBox.Show("Строка была выделена неверно");
            }
        }

        public void Checkout()
        { 
            var cart = new Cart();
            var user = this.context.Users.FirstOrDefault(u => u.Id == CurrentUser.User.Id);
            this.context.Carts.Add(cart);
            if (user != null)
            {
                user.CartId = cart.Id;
                CurrentUser.User = user;
                this.context.SaveChanges();
            }
        }

        public string GetTotalPrice()
        {
            var totalPrice = 0;
            var listOfOrders = this.GetListOfUserOrders();

            foreach (var order in listOfOrders)
            {
                totalPrice = totalPrice + order.TotalPrice;
            }

            return Convert.ToString(totalPrice);
        }

        public List<Order> GetListOfUserOrders()
        {
            var currentCart = CurrentUser.User.Cart.Id;
            var orders = context.Orders.Where(o => o.CartId == currentCart).ToList();
            return orders;
        }
    }
}
