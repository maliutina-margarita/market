﻿using System.Linq;

namespace Market.Repository
{
    using Market.Entity;

    public class ProductInfoRepository
    {
        private readonly Context context = new Context();

        public Product GetProductById(int id)
        {
            return context.Products.FirstOrDefault(p => p.Id == id);
        }

        public void AddProductToProductList(
            string name,
            int count,
            int price,
            string description,
            string provider,
            int weight)
        {
            var product = new Product
                              {
                                  Name = name,
                                  Count = count,
                                  Price = price,
                                  Description = description,
                                  Provider = provider,
                                  Weight = weight
                              };
            this.context.Products.Add(product);
            this.context.SaveChanges();
        }
    }
}
