﻿using System.Collections.Generic;
using System.Linq;
using Market.Core.Enums;

namespace Market.Repository
{
    using System.Windows.Forms;

    using Market.Entity;

    public class ProductRepository
    {
        private readonly Context context = new Context();

        private readonly CartRepository cartRepo = new CartRepository();

        public void DeleteProductName(string selectedProduct)
        {
            var product = this.context.Products.FirstOrDefault(p => p.Name == selectedProduct);
            if (product != null)
            {
                this.context.Products.Remove(product);
                this.context.SaveChanges();
            }
        }

        public List<Product> GetProductsList()
        {
            var productList = this.context.Products.ToList();
            return productList;
        }

        public List<Product> GetProductsList(OrderType type)
        {
            var productList = this.context.Products.ToList();
            return type == OrderType.Asc
                       ? productList.OrderBy(i => i.Name).ToList()
                       : productList.OrderByDescending(i => i.Name).ToList();
        }

        public void AddOrderToCart(string productName, int productCount)
        {
            var cart = cartRepo.GetUserCart();
            var product = this.GetProductByName(productName);
            if (cart != null && product != null)
            {
                var order = this.context.Orders.FirstOrDefault(o => o.CartId == cart.Id && product.Id == o.ProductId);
                if (order != null)
                {
                    order.Count = order.Count + productCount;
                    order.TotalPrice = order.TotalPrice + (product.Price * productCount);
                    product.Count = product.Count - productCount;
                    if (order.Count <= product.Count)
                    {
                        this.context.SaveChanges();
                        MessageBox.Show("Количество продукта увеличено");
                    }
                    else
                    {
                        MessageBox.Show("Превышено количество продукта");
                    }
                }
                else
                {
                    var newOrder = new Order
                    {
                        CartId = cart.Id,
                        Count = productCount,
                        ProductId = product.Id,
                        Product = product
                    };
                    if (newOrder.Count <= product.Count)
                    {
                        product.Count = product.Count - productCount;
                        newOrder.TotalPrice = product.Price * newOrder.Count;
                        this.context.Orders.Add(newOrder);
                        this.context.SaveChanges();
                        MessageBox.Show("Продукт добавлен");
                    }
                }
            }
        }

        public Product GetProductByName(string productName)
        {
            return this.context.Products.FirstOrDefault(p => p.Name == productName);
        }

        public Product GetProductById(int id)
        {
            return this.context.Products.FirstOrDefault(p => p.Id == id);
        }

        public void ChangeProductCount(string name, int newCount)
        {
            var form = new InfoForm();
            form.Show();
        }
    }
}
