﻿namespace Market.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using Market.Entity;

    public class RegistrationRepository
    {
        private readonly Context context = new Context();

        public void Registration(string email, string password, string userType)
        {
            var user = new User { Email = email, Password = password };

            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
            {
                if (!string.IsNullOrEmpty(userType))
                {
                    var cart = new Cart();
                    this.context.Carts.Add(cart);

                    user.UserTyperId = GetUserTypeByName(userType).Id;
                    user.CartId = cart.Id;
                    this.context.Users.Add(user);
                    this.context.SaveChanges();
                    MessageBox.Show("Пользователь добавлен");
                }
                else
                {
                    MessageBox.Show("Выберите тип пользователя");
                }
            }
            else
            {
                MessageBox.Show("Поля заполнены неверно");
            }
        }

        public UserType GetUserType(int id)
        {
            return this.context.UserTypes.FirstOrDefault(u => u.Id == id);
        }

        public UserType GetUserTypeByName(string name)
        {
            return this.context.UserTypes.FirstOrDefault(u => u.Name == name);
        }

        public void CreateDefaultUserType(string type)
        {
            var userTable = context.UserTypes.Any();

            if (userTable == false)
            {
                var typeUser = new UserType { Name = "User" };
                var typeAdmin = new UserType { Name = "Admin" };
                this.context.UserTypes.Add(typeUser);
                this.context.UserTypes.Add(typeAdmin);
                this.context.SaveChanges();
            }
        }

        public List<UserType> GetAllUserTypes()
        {
            return this.context.UserTypes.ToList();
        }
    }
}
