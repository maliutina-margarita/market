﻿namespace Market.Repository
{
    using System.Data.Entity;
    using System.Linq;
    using Market.Entity;

    public class LoginRepository
    {
        private readonly Context context = new Context();

        public User GetUserByLoginAndEmail(string email, string password)
        {
            var user = context.Users
                .Include(i => i.Cart)
                .Include(i => i.UserType)
                .FirstOrDefault(u => u.Email == email && u.Password == password);

            return user;
        }
    }
}
