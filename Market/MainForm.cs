﻿using System;
using System.Windows.Forms;

namespace Market
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Button1Click(object sender, EventArgs e)
        {
            var form = new LoginForm();
            form.Show();
        }

        private void RegistrationButtonClick(object sender, EventArgs e)
        {
            var form = new RegistrationForm();
            form.Show();
        }
    }
}
