﻿using System;
using System.Windows.Forms;

namespace Market
{
    using Market.Core;
    using Market.Repository;

    public partial class CartForm : Form
    {
        private readonly CartRepository repository = new CartRepository();

        public CartForm()
        {
            InitializeComponent();

            this.CartTable.DataSource = this.repository.GetListOfOrders();

            this.label2.Text = "Для удаления нужно" + "\n" + "выбрать ячейку с Id";

            this.TotalPriceLabel.Text = this.repository.GetTotalPrice();
        }

        private void OrderButtonClick(object sender, EventArgs e)
        {
            this.repository.Checkout();
            this.Close();
        }

        private void DeleteButtonClick(object sender, EventArgs e)
        {
            this.repository.DeleteOrderFromCartTable((int)this.CartTable.CurrentCell.Value);
            this.CartTable.DataSource = this.repository.GetListOfOrders();
        }
    }
}
