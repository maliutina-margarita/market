﻿using System.Collections.Generic;

namespace Market.Entity
{
    public class Cart
    {
        public Cart()
        {
            Orders = new List<Order>();
        }

        public int Id { get; set; }

        public List<Order> Orders { get; set; }
    }
}
