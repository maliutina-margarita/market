﻿namespace Market.Entity
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class User
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public Cart Cart { get; set; }

        [ForeignKey("Cart")]
        public int CartId { get; set; }
        
        public UserType UserType { get; set; }

        [ForeignKey("UserType")]
        public int UserTyperId { get; set; }
    }
}
