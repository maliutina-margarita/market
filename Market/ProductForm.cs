﻿using System;
using System.Linq;
using System.Windows.Forms;
using Market.Core.Enums;
using Market.Repository;

namespace Market
{
    using Market.Core;
    using Market.Entity;

    public partial class ProductsForm : Form
    {
        private readonly ProductRepository productRepos = new ProductRepository();
     
        public ProductsForm()
        {
            InitializeComponent();

            ProductListBox.DataSource = productRepos.GetProductsList();
            ProductListBox.DisplayMember = "Name";
            ProductListBox.ValueMember = "Id";

            productComboBox.DataSource = productRepos.GetProductsList();
            productComboBox.DisplayMember = "Name";
            productComboBox.ValueMember = "Id";

            sortComboBox.Items.AddRange(new[] { "По возрастанию", "По убыванию" });

            this.UserTypeLabel.Text = CurrentUser.User.UserType.Name;

            if (CurrentUser.User.UserType.Name != "Admin")
            {
                this.deleteButton.Hide();
                this.productComboBox.Hide();
                this.addButton.Hide();
            }
            else
            {
                this.AddCountLabel.Text = "Add count";
            }
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            var form = new InfoForm();
            form.Show();

            ProductListBox.DataSource = productRepos.GetProductsList();
            ProductListBox.DisplayMember = "Name";
            ProductListBox.ValueMember = "Id";

            productComboBox.DataSource = productRepos.GetProductsList();
            productComboBox.DisplayMember = "Name";
            productComboBox.ValueMember = "Id";
        }

        private void DeleteButtonClick(object sender, EventArgs e)
        {
            productRepos.DeleteProductName(productComboBox.Text);
            MessageBox.Show("Продукт удален");

            ProductListBox.DataSource = productRepos.GetProductsList();
            ProductListBox.DisplayMember = "Name";
            ProductListBox.ValueMember = "Id";

            productComboBox.DataSource = productRepos.GetProductsList();
            productComboBox.DisplayMember = "Name";
            productComboBox.ValueMember = "Id";
        }

        private void SortButtonClick(object sender, EventArgs e)
        {
            ProductListBox.DataSource = productRepos.GetProductsList(sortComboBox.SelectedItem.ToString() == "По возрастанию" ? OrderType.Asc : OrderType.Desc);
        }

        private void SearchTextBoxTextChanged(object sender, EventArgs e)
        {
            ProductListBox.DataSource = productRepos.GetProductsList().Where(p => p.Name.ToLower().Contains(searchTextBox.Text.ToLower())).ToList();
        }

        private void RefreshButtonClick(object sender, EventArgs e)
        {
            ProductListBox.DataSource = productRepos.GetProductsList();
            ProductListBox.DisplayMember = "Name";
            ProductListBox.ValueMember = "Id";
        }

        private void CartButtonClick(object sender, EventArgs e)
        {
            var form = new CartForm();
            form.Show();
        }

        private void AddToCartButtonClick(object sender, EventArgs e)
        {
            this.productRepos.AddOrderToCart(this.ProductListBox.Text, Convert.ToInt32(this.CountNumber.Value));
        }

        private void ProductListBoxMouseClick(object sender, MouseEventArgs e)
        {
           var product = (Product)ProductListBox.SelectedItem;

           this.CountLabel.Text = product.Count + string.Empty;

           // var productFromDb  = this.productRepos.GetProductById((int)ProductListBox.SelectedValue);
        }

        private void CountNumberValueChanged(object sender, EventArgs e)
        {
            var product = (Product)ProductListBox.SelectedItem;
            this.CountNumber.Maximum = product.Count;
            this.CountNumber.Minimum = 0;
        }

        private void ProductListBoxMouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (CurrentUser.User.UserType.Name == "Admin")
            {
                this.productRepos.ChangeProductCount(ProductListBox.Text, Convert.ToInt32(CountNumber.Value));
            }
        }
    }
}
