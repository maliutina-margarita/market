﻿using System;
using System.Windows.Forms;
using Market.Core;

namespace Market
{
    using Market.Repository;

    public partial class LoginForm : Form
    {
        private readonly LoginRepository repository = new LoginRepository();

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LogButtonClick(object sender, EventArgs e)
        {
            var user = this.repository.GetUserByLoginAndEmail(this.EmailTextBox.Text, this.PasswordTextBox.Text);

            if (user != null)
            {
                CurrentUser.User = user;
                var form = new ProductsForm();
                form.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Такого пользователя не существует");
            }
        }
    }
}
